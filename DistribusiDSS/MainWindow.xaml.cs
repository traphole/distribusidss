﻿using Excel = Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Threading;
using System.Collections.Specialized;
using System.ComponentModel;

namespace DistribusiDSS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void FirstLoad(object sender, RoutedEventArgs e)
        {
            System.Windows.Data.CollectionViewSource distribusiViewSource =
                ((System.Windows.Data.CollectionViewSource)(this.FindResource("distribusiViewSource")));
            distribusiViewSource.Source = Distribusi.Instance;
            System.Windows.Data.CollectionViewSource tabelKontrolViewSource =
                ((System.Windows.Data.CollectionViewSource)(this.FindResource("tabelKontrolViewSource")));
            tabelKontrolViewSource.Source = TabelKontrol.Instance;
            // Set default sort of tabelKontrolDataGrid
            tabelKontrolDataGrid.Columns[0].SortDirection = ListSortDirection.Ascending;
            tabelKontrolDataGrid.Items.SortDescriptions.Add(
                new SortDescription(tabelKontrolDataGrid.Columns[0].SortMemberPath, ListSortDirection.Ascending));

            Distribusi.Instance.CollectionChanged += new NotifyCollectionChangedEventHandler(OnChangeDistribusiUpdateTabelKontrol);

            DistribusiBaru dialog_distribusiBaru = new DistribusiBaru();
            dialog_distribusiBaru.Owner = this;
            Nullable<bool> result = dialog_distribusiBaru.ShowDialog();
            UpdateTabelKontrol();
            UpdateRasioBentrokMain();
        }

        private void OnChangeDistribusiUpdateTabelKontrol(object sender, NotifyCollectionChangedEventArgs args)
        {
            if (!DistribusiFuzzyMCDM.generatingDistribusi)
            {
                UpdateTabelKontrol();
                UpdateRasioBentrokMain();
            }
        }

        public void OnChangeDistribusiUpdateTabelKontrol(object sender, PropertyChangedEventArgs args)
        {
            try
            {
                if (args.PropertyName == "listAngklung" || args.PropertyName == "setAngklung")
                {
                    UpdateTabelKontrol();
                    UpdateRasioBentrokMain();
                }
                else if (args.PropertyName == "namaPemain")
                {
                    UpdateTabelKontrol();
                }
            }
            catch (KeyNotFoundException)
            {
                MessageBox.Show("Angklung tidak ada dalam partitur.");
                return;
            }
        }

        private void UpdateTabelKontrol()
        {
            TabelKontrol.Instance.Clear();
            foreach (Pemain p in Distribusi.Instance)
            {
                foreach (Angklung a in p.setAngklung)
                {
                    int idx = TabelKontrol.Instance.CariAngklung(a);
                    if (idx == -1)
                    {
                        KontrolAngklung k = new KontrolAngklung(a);
                        k.jumlah++;
                        k.daftarPemainSet.Add(p.namaPemain);
                        TabelKontrol.Instance.Add(k);
                    }
                    else
                    {
                        TabelKontrol.Instance[idx].jumlah++;
                        TabelKontrol.Instance[idx].daftarPemainSet.Add(p.namaPemain);
                    }
                }
            }
        }

        private void UpdateRasioBentrokMain()
        {
            foreach (Pemain p in Distribusi.Instance)
            {
                Tuple<double, double> rasioBentrokMain = DistribusiFuzzyMCDM.CountRasioBentrokMain(p.setAngklung);
                p.rasioBentrok = rasioBentrokMain.Item1;
                p.rasioMain = rasioBentrokMain.Item2;
            }
        }

        private void SimpanDistribusi(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".xlsx";
            dlg.Filter = "Excel Spreadsheet (*.xls, *.xlsx, *.xlsb)|*.xls;*.xlsx;*.xlsb";
            Nullable<bool> result = dlg.ShowDialog();
            if (result != true)
            {
                return;
            }
            Excel.Application excelApp = new Excel.Application();
            excelApp.Workbooks.Add();
            Excel.Worksheet worksheet = excelApp.ActiveSheet;
            worksheet.Cells[1, 1] = "Nama Pemain";
            worksheet.Cells[1, 2] = "Distribusi";
            int colmax = 2;
            for (int p = 0; p < Distribusi.Instance.Count; p++)
            {
                worksheet.Cells[p + 2, 1] = Distribusi.Instance[p].namaPemain;
                int col = 2;
                foreach (Angklung angk in Distribusi.Instance[p].setAngklung)
                {
                    worksheet.Cells[p + 2, col] = angk.ToString();
                    if (colmax < col) colmax = col;
                    col++;
                }
            }
            worksheet.Range[worksheet.Cells[1, 2], worksheet.Cells[1, colmax]].Merge();
            worksheet.Columns[1].AutoFit();
            Excel.Range all = worksheet.Range[worksheet.Cells[1, 1], worksheet.Cells[Distribusi.Instance.Count + 1, colmax]];
            all.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
            worksheet.Cells[1, 2].HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            for (int i = 2; i <= colmax; i++)
            {
                worksheet.Columns[i].ColumnWidth = 3;
            }
            worksheet.SaveAs(dlg.FileName);
            excelApp.Quit();
            MessageBox.Show("Distribusi selesai disimpan.", "Penyimpanan berhasil");
        }

        private void DistribusiBaru_Click(object sender, RoutedEventArgs e)
        {
            Distribusi.Instance.Clear();
            TabelKontrol.Instance.Clear();
            PartiturCollection.Instance.Clear();
            DistribusiBaru dialog_distribusiBaru = new DistribusiBaru();
            dialog_distribusiBaru.Owner = this;
            Nullable<bool> result = dialog_distribusiBaru.ShowDialog();
            UpdateTabelKontrol();
            UpdateRasioBentrokMain();
        }

        private void Keluar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TambahPemain_Click(object sender, RoutedEventArgs e)
        {
            Pemain newPemain = new Pemain();
            newPemain.PropertyChanged += this.OnChangeDistribusiUpdateTabelKontrol;
            Distribusi.Instance.Add(newPemain);
        }

        private void HapusPemain_Click(object sender, RoutedEventArgs e)
        {
            Distribusi.Instance.Remove((Pemain)distribusiDataGrid.SelectedItem);
        }

        private void InformasiPartitur_Click(object sender, RoutedEventArgs e)
        {
            InformasiPartiturWindow ipw = new InformasiPartiturWindow();
            ipw.Owner = this;
            ipw.Show();
        }
    }
}
