﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DistribusiDSS
{
    /// <summary>
    /// Interaction logic for InformasiPartiturWindow.xaml
    /// </summary>
    public partial class InformasiPartiturWindow : Window
    {
        public InformasiPartiturWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            System.Windows.Data.CollectionViewSource partiturCollectionViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("partiturCollectionViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            partiturCollectionViewSource.Source = PartiturCollection.Instance;

            NadaDasar_Label.Content = ((Partitur)partiturCollectionListView.SelectedItem).NadaDasar;
            AngklungDibutuhkan_Label.Text = ((Partitur)partiturCollectionListView.SelectedItem).AngklungDibutuhkan;
            partiturCollectionListView.SelectionChanged += partiturCollectionListView_SelectionChanged;
        }

        void partiturCollectionListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NadaDasar_Label.Content = ((Partitur)partiturCollectionListView.SelectedItem).NadaDasar;
            AngklungDibutuhkan_Label.Text = ((Partitur)partiturCollectionListView.SelectedItem).AngklungDibutuhkan;
        }
    }
}
