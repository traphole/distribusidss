﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistribusiDSS
{
    public class Partitur
    {
        public readonly List<List<string>> PartiturRelatif;
        public readonly List<List<Angklung>> PartiturAbsolut;
        private string partiturOriginPath;

        public bool isRelatifProcessed;
        public bool isAbsolutProcessed;

        public readonly HashSet<string> nadaDasar;
        public readonly HashSet<Angklung> angklungDibutuhkan;

        public Partitur(string partiturPath)
        {
            PartiturRelatif = new List<List<string>>();
            PartiturAbsolut = new List<List<Angklung>>();
            partiturOriginPath = partiturPath;

            isRelatifProcessed = false;
            isAbsolutProcessed = false;

            nadaDasar = new HashSet<string>();
            angklungDibutuhkan = new HashSet<Angklung>();
        }

        public string PartiturOriginPath
        {
            get { return partiturOriginPath; }
        }

        public string NadaDasar
        {
            get { return String.Join(", ", nadaDasar); }
        }

        public string AngklungDibutuhkan
        {
            get
            {
                List<Angklung> adb = new List<Angklung>(angklungDibutuhkan);
                adb.Sort();
                return String.Join(", ", adb);
            }
        }
    }
}
