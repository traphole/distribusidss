﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistribusiDSS
{
    public sealed class PartiturCollection : ObservableCollection<Partitur>
    {
        public static readonly PartiturCollection Instance = new PartiturCollection();
        static PartiturCollection() { }
        private PartiturCollection() : base() { }

        public static bool parseError;
    }
}
