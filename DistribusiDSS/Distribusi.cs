﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DistribusiDSS
{
    public sealed class Distribusi : ObservableCollection<Pemain>
    {
        public static readonly Distribusi Instance = new Distribusi();
        static Distribusi() { }
        private Distribusi() : base() { }
    }
}
