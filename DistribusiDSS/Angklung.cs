﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistribusiDSS
{
    public class Angklung : IEquatable<Angklung>, IComparable, IComparable<Angklung>
    {
        private static string[] urutanAngklung = new string[] {
            // Cg = -18
            "Cg", "C#g", "Dg", "D#g", "Eg", "Fg", "F#g",
            // G = -11
            "G", "G#", "A", "A#", "B", "C", "C#", "D", "D#", "E", "F"
        };

        private string noAngklungString;

        public string noAngklung
        {
            get { return noAngklungString; }
        }

        public int nilaiAngklung
        {
            get
            {
                int a_int;
                if (int.TryParse(this.noAngklung, out a_int))
                {
                    return a_int;
                }
                else
                {
                    return Array.IndexOf<String>(Angklung.urutanAngklung, this.noAngklung) - Angklung.urutanAngklung.Length;
                }
            }
        }

        public Angklung(string nomor)
        {
            this.noAngklungString = nomor;
        }
        public Angklung(int nomor)
        {
            this.noAngklungString = nomor.ToString();
        }
        public Angklung(Angklung other)
        {
            this.noAngklungString = other.noAngklung;
        }

        public int CompareTo(Angklung otherAngklung)
        {
            if (otherAngklung == null) return 1;

            int a1, a2;
            bool a1_int = int.TryParse(this.noAngklung, out a1);
            bool a2_int = int.TryParse(otherAngklung.noAngklung, out a2);
            if (!a1_int)
            {
                a1 = Array.IndexOf<String>(Angklung.urutanAngklung, this.noAngklung) - Angklung.urutanAngklung.Length;
            }
            if (!a2_int)
            {
                a2 = Array.IndexOf<String>(Angklung.urutanAngklung, otherAngklung.noAngklung) - Angklung.urutanAngklung.Length;
            }
            return a1.CompareTo(a2);
        }

        public bool Equals(Angklung other)
        {
            if (other == null) return false;
            if (this.noAngklung == other.noAngklung)
                return true;
            else
                return false;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Angklung otherAngklung = obj as Angklung;
            if (otherAngklung != null)
                return CompareTo(otherAngklung);
            else
                return 1;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            Angklung otherAngklung = obj as Angklung;
            if (otherAngklung != null)
                return Equals(otherAngklung);
            else
                return false;
        }

        public static bool operator ==(Angklung a1, Angklung a2)
        {
            if ((object)a1 == null || (object)a2 == null)
                return Object.Equals(a1, a2);

            return a1.Equals(a2);
        }

        public static bool operator !=(Angklung a1, Angklung a2)
        {
            if ((object)a1 == null || (object)a2 == null)
                return !Object.Equals(a1, a2);

            return !a1.Equals(a2);
        }

        public static bool operator <(Angklung a1, Angklung a2)
        {
            return a1.CompareTo(a2) < 0;
        }

        public static bool operator <=(Angklung a1, Angklung a2)
        {
            return a1.CompareTo(a2) <= 0;
        }

        public static bool operator >(Angklung a1, Angklung a2)
        {
            return a1.CompareTo(a2) > 0;
        }

        public static bool operator >=(Angklung a1, Angklung a2)
        {
            return a1.CompareTo(a2) >= 0;
        }

        public override int GetHashCode()
        {
            return this.noAngklung.GetHashCode();
        }

        public override string ToString()
        {
            return this.noAngklung;
        }
    }
}
