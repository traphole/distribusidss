﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DistribusiDSS
{
    /// <summary>
    /// Interaction logic for DistribusiBaru.xaml
    /// </summary>
    public partial class DistribusiBaru : Window
    {
        //private const int GWL_STYLE = -16;
        //private const int WS_SYSMENU = 0x80000;
        //[DllImport("user32.dll", SetLastError = true)]
        //private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        //[DllImport("user32.dll")]
        //private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        ProgressWindow ProgWin;

        public DistribusiBaru()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //var hwnd = new WindowInteropHelper(this).Handle;
            //SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);

            System.Windows.Data.CollectionViewSource partiturCollectionViewSource =
                ((System.Windows.Data.CollectionViewSource)(this.FindResource("partiturCollectionViewSource")));
            partiturCollectionViewSource.Source = PartiturCollection.Instance;
        }

        private void cariFilePartitur(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".xlsx";
            dlg.Filter = "Excel Spreadsheet (*.xls, *.xlsx, *.xlsb)|*.xls;*.xlsx;*.xlsb";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                PartiturCollection.Instance.Add(new Partitur(dlg.FileName));
            }
        }

        private void hapusFilePartitur(object sender, RoutedEventArgs e)
        {
            Partitur selected = (Partitur)partiturCollectionListView.SelectedItem;
            PartiturCollection.Instance.Remove(selected);
        }

        private void SelesaiForm_Click(object sender, RoutedEventArgs e)
        {
            if (PartiturCollection.Instance.Count == 0)
            {
                MessageBox.Show("Mohon masukkan minimal satu buah partitur.");
                return;
            }
            if (String.IsNullOrWhiteSpace(JmlPemain_TxtBox.Text))
            {
                MessageBox.Show("Mohon masukkan jumlah pemain dalam tim.");
                return;
            }
            int j;
            if (!int.TryParse(JmlPemain_TxtBox.Text, out j))
            {
                MessageBox.Show("Mohon masukkan angka sebagai jumlah pemain.");
                JmlPemain_TxtBox.Text = "";
                return;
            }
            if (j > 100)
            {
                MessageBox.Show("Jumlah pemain maksimal 100 orang.");
                JmlPemain_TxtBox.Text = "";
                return;
            }
            if (j < 5)
            {
                MessageBox.Show("Jumlah pemain minimal 5 orang.");
                JmlPemain_TxtBox.Text = "";
                return;
            }

            ProgWin = new ProgressWindow();
            ProgWin.Show();
            ProgWin.Indeterminate();
            PartiturCollection.parseError = false;
            foreach (Partitur part in PartiturCollection.Instance)
            {
                PartiturExcelParser pep = new PartiturExcelParser(part);
                BackgroundWorker pep_bw = new BackgroundWorker();
                pep_bw.WorkerReportsProgress = false;
                pep_bw.WorkerSupportsCancellation = false;
                pep_bw.RunWorkerCompleted += pep_Completed;
                pep_bw.DoWork += pep_DoWork;
                pep_bw.RunWorkerAsync(pep);
            }
        }

        private void pep_DoWork(object sender, DoWorkEventArgs eargs)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;

            PartiturExcelParser pep = (PartiturExcelParser)eargs.Argument;
            pep.ConvertExcelToPartiturRelatif(worker, eargs);
            pep.ConvertPartiturRelatifToAbsolut(worker, eargs);
        }

        private void pep_Completed(object sender, RunWorkerCompletedEventArgs eargs)
        {
            if (eargs.Error != null)
            {
                MessageBox.Show("Error: " + eargs.Error.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (PartiturCollection.parseError)
            {
                MessageBox.Show("Gagal membaca partitur: kemungkinan nada dasar belum ditentukan, atau ada kesalahan format partitur.",
                    "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                ProgWin.Close();
                return;
            }

            foreach (Partitur p in PartiturCollection.Instance)
            {
                if (!p.isAbsolutProcessed) return;
            }
            //MessageBox.Show("Parse all partitur selesai. Generating distribusi...");
            ProgWin.Determinate();

            DistribusiFuzzyMCDM dfm = new DistribusiFuzzyMCDM(int.Parse(JmlPemain_TxtBox.Text));
            BackgroundWorker dfm_bw = new BackgroundWorker();
            dfm_bw.WorkerReportsProgress = true;
            dfm_bw.WorkerSupportsCancellation = false;
            dfm_bw.RunWorkerCompleted += dfm_Completed;
            dfm_bw.ProgressChanged += dfm_Progress;
            dfm_bw.DoWork += dfm_DoWork;
            dfm_bw.RunWorkerAsync(dfm);
        }

        private void dfm_DoWork(object sender, DoWorkEventArgs eargs)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;

            DistribusiFuzzyMCDM dfm = (DistribusiFuzzyMCDM)eargs.Argument;

            DistribusiFuzzyMCDM.generatingDistribusi = true;
            eargs.Result = dfm.GenerateDistribusi_v2(worker, eargs);
        }

        private void dfm_Progress(object sender, ProgressChangedEventArgs eargs)
        {
            this.Dispatcher.Invoke(new Action(() => ProgWin.OnProgress(eargs.ProgressPercentage)));
        }

        private void dfm_Completed(object sender, RunWorkerCompletedEventArgs eargs)
        {
            if (eargs.Error != null)
            {
                MessageBox.Show("Error: " + eargs.Error.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            List<Pemain> hasilDistribusi = (List<Pemain>)eargs.Result;
            Distribusi.Instance.Clear();
            foreach (Pemain p in hasilDistribusi)
            {
                Pemain newPemain = new Pemain(p);
                newPemain.PropertyChanged += ((MainWindow)this.Owner).OnChangeDistribusiUpdateTabelKontrol;
                Distribusi.Instance.Add(newPemain);
            }
            //MessageBox.Show("Generate distribusi selesai.");
            DistribusiFuzzyMCDM.generatingDistribusi = false;
            ProgWin.Close();
            this.DialogResult = true;
        }
    }
}
