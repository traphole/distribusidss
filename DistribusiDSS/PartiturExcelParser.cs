﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.IO.IsolatedStorage;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Reflection;
using System.Windows.Resources;
using System.ComponentModel;
using System.Threading;

namespace DistribusiDSS
{
    class PartiturExcelParser
    {
        const string NOTANGKA_ACCEPTED_CHARS = "[^zxcvbnmZXCVBNMqwertyuQWERTYUIOP{}sdghjSDGHJ23567@#%^&()+.]";
        int[] NOT_TO_ABS = new int[] { 6, 8, 10, 11, 13, 15, 17 };
        List<string> NADA_DASAR_DIFF = new List<string>(new string[] { "G", "G#", "A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#" });
        List<string> NEG_TO_ABS = new List<string>(new string[] {"Cg", "C#g", "Dg", "D#g", "Eg", "Fg", "F#g",
            "G", "G#", "A", "A#", "B", "C", "C#", "D", "D#", "E", "F"
        });
        
        Partitur partitur;

        public PartiturExcelParser(Partitur partiturToProcess)
        {
            this.partitur = partiturToProcess;
        }

        public void ConvertExcelToPartiturRelatif(BackgroundWorker worker, DoWorkEventArgs eargs)
        {
            //worker.ReportProgress(1);
            Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            Workbook wb = excelApp.Workbooks.Open(partitur.PartiturOriginPath);
            Worksheet sheet = wb.Sheets[1];
            Range used = sheet.UsedRange;
            object[,] partiturExcel = (object[,])used.get_Value();

            int index = 0, bigIndex = 0;
            string lastProcessedNotA = "";
            Dictionary<string, string> dict_notangka = ReadDictionary();
            List<List<string>> partiturResult = partitur.PartiturRelatif;
            partiturResult.Add(new List<string>());

            //float prog = 0;
            float total = partiturExcel.GetLength(0);

            for (int row = 1; row <= total; row++)
            {
                bool isEmptyRow = true;
                for (int col = 1; col <= partiturExcel.GetLength(1); col++)
                {
                    if (partiturExcel[row, col] != null)
                    {
                        isEmptyRow = false;
                        break;
                    }
                }
                if (isEmptyRow)
                {
                    if (partiturResult.Count > 0)
                    {
                        bigIndex += partiturExcel.GetLength(1);
                        index = bigIndex;
                    }
                    continue;
                }
                else if (partiturExcel[row, 1] is string && ((string)partiturExcel[row, 1]).ToLower().Replace(" ", "").StartsWith("do="))
                {
                    if (index >= partiturResult.Count)
                    {
                        partiturResult.Add(new List<string>());
                    }
                    string nadadasar = ((string)partiturExcel[row, 1]).ToLower().Replace(" ", "");
                    nadadasar = nadadasar.Substring(0, Math.Min(nadadasar.Length, 6));
                    if (nadadasar.Substring(4) == "#" || nadadasar.Substring(4) == "b")
                    {
                        nadadasar = nadadasar.Substring(0, 5);
                    }
                    else if (nadadasar.Substring(4) == "is")
                    {
                        nadadasar = nadadasar.Substring(0, 4) + "#";
                    }
                    else if (nadadasar.Substring(4) == "es")
                    {
                        nadadasar = nadadasar.Substring(0, 4) + "b";
                    }
                    else
                    {
                        nadadasar = nadadasar.Substring(0, 4);
                    }
                    partiturResult[index].Add(nadadasar);
                    index++; bigIndex++;
                    continue;
                }
                for (int col = 1; col <= partiturExcel.GetLength(1); col++)
                {
                    string cellValue;
                    if (partiturExcel[row, col] is double)
                    {
                        double val = (double)partiturExcel[row, col];
                        if (val == 0.0)
                            cellValue = String.Empty;
                        else
                            cellValue = Convert.ToInt32(val).ToString();
                    }
                    else if (partiturExcel[row, col] is string)
                    {
                        cellValue = (string)partiturExcel[row, col];
                    }
                    else if (partiturExcel[row, col] == null)
                    {
                        cellValue = String.Empty;
                    }
                    else
                    {
                        throw new Exception("dafuq is this? unknown type.");
                    }
                    if (index >= partiturResult.Count)
                    {
                        partiturResult.Add(new List<string>());
                    }
                    string notHuruf = Regex.Replace(cellValue, NOTANGKA_ACCEPTED_CHARS, String.Empty);
                    string notAngka;
                    foreach (char c in notHuruf)
                    {
                        if (c == '.')
                        {
                            notAngka = lastProcessedNotA;
                        }
                        else
                        {
                            notAngka = dict_notangka[c.ToString()];
                        }
                        partiturResult[index].Add(notAngka);
                        lastProcessedNotA = notAngka;
                    }
                    index++;
                }
                index = bigIndex;

                //prog = ((row / total) * 49) + 1;
                //worker.ReportProgress((int)prog);
            }
            excelApp.Quit();
            partitur.isRelatifProcessed = true;
        }

        public void ConvertPartiturRelatifToAbsolut(BackgroundWorker worker, DoWorkEventArgs eargs)
        {
            List<List<Angklung>> partiturAbsolut = partitur.PartiturAbsolut;
            string nadaDasar = "";
            //float prog = 0;
            int total = partitur.PartiturRelatif.Count;
            for (int i = 0; i < total; i++)
            {
                List<string> baris = partitur.PartiturRelatif[i];
                if (baris.Count == 0)
                {
                    partiturAbsolut.Add(new List<Angklung>());
                    continue;
                }
                if (baris[0].StartsWith("do="))
                {
                    nadaDasar = baris[0].Substring(3).ToUpper();
                    if (nadaDasar.EndsWith("b"))
                    {
                        switch (nadaDasar)
                        {
                            case "Cb":
                                nadaDasar = "B";
                                break;
                            case "Db":
                                nadaDasar = "C#";
                                break;
                            case "Eb":
                                nadaDasar = "D#";
                                break;
                            case "Fb":
                                nadaDasar = "E";
                                break;
                            case "Gb":
                                nadaDasar = "F#";
                                break;
                            case "Ab":
                                nadaDasar = "G#";
                                break;
                            case "Bb":
                                nadaDasar = "A#";
                                break;
                        }
                    }
                    partitur.nadaDasar.Add(nadaDasar);
                }
                else
                {
                    partiturAbsolut.Add(new List<Angklung>());
                    foreach (string na in baris)
                    {
                        Stack<char> notAngka = new Stack<char>(na.Reverse());
                        char angka = notAngka.Pop();
                        int nada = NOT_TO_ABS[int.Parse(angka.ToString()) - 1];
                        if (NADA_DASAR_DIFF.IndexOf(nadaDasar) == -1)
                        {
                            PartiturCollection.parseError = true;
                            return;
                        }
                        nada += NADA_DASAR_DIFF.IndexOf(nadaDasar) - 5;
                        while (notAngka.Count > 0)
                        {
                            char token = notAngka.Pop();
                            switch (token)
                            {
                                case '/':
                                    nada += 1;
                                    break;
                                case '\\':
                                    nada -= 1;
                                    break;
                                case '.':
                                    nada -= 12;
                                    break;
                                case '\'':
                                    nada += 12;
                                    break;
                                default:
                                    throw new Exception("Unkown token found on line " + na);
                            }
                        }
                        if (nada < 0)
                        {
                            if (nada > -19)
                            {
                                string nadaS = NEG_TO_ABS[nada + 18];
                                partiturAbsolut[partiturAbsolut.Count - 1].Add(new Angklung(nadaS));
                                partitur.angklungDibutuhkan.Add(new Angklung(nadaS));
                            }
                            else
                            {
                                throw new Exception("Nada out of range on line " + na);
                            }
                        }
                        else
                        {
                            partiturAbsolut[partiturAbsolut.Count - 1].Add(new Angklung(nada.ToString()));
                            partitur.angklungDibutuhkan.Add(new Angklung(nada.ToString()));
                        }
                    }
                }
                //prog = ((((float)i + 1) / total) * 50) + 50;
                //worker.ReportProgress((int)prog);
            }
            partitur.isAbsolutProcessed = true;
        }

        private Dictionary<string, string> ReadDictionary()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            StreamResourceInfo info = System.Windows.Application.GetResourceStream(new Uri("dictionary.json", UriKind.Relative));
            using (StreamReader reader = new StreamReader(info.Stream))
            {
                JObject obj = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
                foreach (JProperty entry in obj.Properties())
                {
                    result.Add(entry.Name, (string)entry.Value);
                }
            }
            return result;
        }
    }
}
