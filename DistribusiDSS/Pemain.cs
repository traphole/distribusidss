﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DistribusiDSS
{
    public class Pemain : INotifyPropertyChanged
    {
        private string namaPemainVal;
        private HashSet<Angklung> setAngklungVal;
        private double rasioBentrokVal;
        private double rasioMainVal;

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public Pemain(string nama, HashSet<Angklung> angklung)
        {
            this.namaPemainVal = nama;
            this.setAngklungVal = new HashSet<Angklung>(angklung);
        }
        public Pemain(Pemain lain)
        {
            this.namaPemainVal = lain.namaPemain;
            this.setAngklungVal = new HashSet<Angklung>(lain.setAngklung);
        }
        public Pemain()
        {
            this.namaPemainVal = "";
            this.setAngklungVal = new HashSet<Angklung>();
        }

        public string namaPemain
        {
            get { return this.namaPemainVal; }
            set
            {
                if (value != this.namaPemainVal)
                {
                    this.namaPemainVal = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public HashSet<Angklung> setAngklung
        {
            get { return this.setAngklungVal; }
            set
            {
                if (!value.SetEquals(setAngklungVal))
                {
                    this.setAngklungVal = new HashSet<Angklung>(value);
                    NotifyPropertyChanged();
                }
            }
        }

        public string listAngklung
        {
            get
            {
                List<Angklung> listAngklung = new List<Angklung>(setAngklungVal);
                listAngklung.Sort();
                return String.Join(", ", listAngklung);
            }
            set
            {
                string inp = Regex.Replace(value, @"[^cdefgabCDEFGAB1234567890#iesIES, ]", String.Empty);
                HashSet<Angklung> newSetAngklung = new HashSet<Angklung>();
                foreach (string angk in inp.Split(','))
                {
                    if (!String.IsNullOrWhiteSpace(angk))
                    {
                        newSetAngklung.Add(new Angklung(angk.Trim()));
                    }
                }
                this.setAngklung = newSetAngklung;
            }
        }

        public double rasioBentrok
        {
            get { return this.rasioBentrokVal; }
            set
            {
                if (value != this.rasioBentrokVal)
                {
                    this.rasioBentrokVal = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public double rasioMain
        {
            get { return this.rasioMainVal; }
            set
            {
                if (value != this.rasioMainVal)
                {
                    this.rasioMainVal = value;
                    NotifyPropertyChanged();
                }
            }
        }
    }
}
