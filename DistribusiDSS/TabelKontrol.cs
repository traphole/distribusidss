﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DistribusiDSS
{
    public sealed class TabelKontrol : ObservableCollection<KontrolAngklung>
    {
        public static readonly TabelKontrol Instance = new TabelKontrol();
        static TabelKontrol() { }
        private TabelKontrol() : base() { }

        public int CariAngklung(Angklung angklung)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].angklung == new Angklung(angklung))
                {
                    return i;
                }
            }
            return -1;
        }
    }

    public class KontrolAngklung : INotifyPropertyChanged
    {
        private Angklung angklungVal;
        private int jumlahVal;
        private HashSet<string> daftarPemainVal;

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public KontrolAngklung(Angklung angklung)
        {
            this.angklungVal = new Angklung(angklung);
            this.jumlahVal = 0;
            this.daftarPemainVal = new HashSet<string>();
        }

        public Angklung angklung
        {
            get { return angklungVal; }
        }

        public int jumlah
        {
            get { return jumlahVal; }
            set
            {
                if (value != jumlahVal)
                {
                    this.jumlahVal = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public string daftarPemain
        {
            get { return String.Join(", ", daftarPemainVal); }
        }

        public HashSet<string> daftarPemainSet
        {
            get { return daftarPemainVal; }
        }
    }
}
