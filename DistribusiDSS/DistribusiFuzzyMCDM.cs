﻿using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Resources;

namespace DistribusiDSS
{
    class DistribusiFuzzyMCDM
    {
        int jmlPemain;

        public static bool generatingDistribusi { get; set; }

        public DistribusiFuzzyMCDM(int jmlPemain)
        {
            this.jmlPemain = jmlPemain;
        }

        public List<Pemain> GenerateDistribusi_v2(BackgroundWorker worker, DoWorkEventArgs eargs)
        {
            // gabung semua partitur
            Partitur partiturToProcess = new Partitur("");
            foreach (Partitur p in PartiturCollection.Instance)
            {
                partiturToProcess.PartiturAbsolut.AddRange(p.PartiturAbsolut);
                partiturToProcess.isAbsolutProcessed = true;
            }

            // hitung jumlah main dan bangun tabel untuk mengecek setiap lokasi
            Dictionary<Angklung, int> tabelJumlahMain = new Dictionary<Angklung, int>();
            //LinkedList<List<Tuple<Angklung, bool>>> partiturCheck = new LinkedList<List<Tuple<Angklung, bool>>>();
            for (int ketukIdx = 0; ketukIdx < partiturToProcess.PartiturAbsolut.Count; ketukIdx++)
            {
                //List<Tuple<Angklung, bool>> newKetuk = new List<Tuple<Angklung, bool>>();
                foreach (Angklung nada in partiturToProcess.PartiturAbsolut[ketukIdx])
                {
                    if (!tabelJumlahMain.ContainsKey(nada)) tabelJumlahMain.Add(nada, 0);
                    tabelJumlahMain[nada]++;
                    //newKetuk.Add(new Tuple<Angklung, bool>(nada, false));
                }
                //if (newKetuk.Count > 0) partiturCheck.AddLast(newKetuk);
            }

            // cari jumlah main maksimal untuk membagi menjadi tiga kategori
            // jarang main, agak sering main, sering main
            int maxJmlMain = 0;
            foreach (int jmlMain in tabelJumlahMain.Values)
            {
                if (maxJmlMain < jmlMain) maxJmlMain = jmlMain;
            }

            // bikin tabel kontrol
            Dictionary<Angklung, int> tabelKontrol = new Dictionary<Angklung, int>();
            foreach (Angklung a in tabelJumlahMain.Keys)
            {
                double jmlAngklung = 0;
                if (a.nilaiAngklung < -11)
                {
                    jmlAngklung += 1;
                }
                else if (a.nilaiAngklung < 0)
                {
                    jmlAngklung += 2;
                }
                else if (a.nilaiAngklung < 20)
                {
                    jmlAngklung += 4;
                }
                else if (a.nilaiAngklung < 30)
                {
                    jmlAngklung += 4;
                }
                else
                {
                    jmlAngklung += 3;
                }
                jmlAngklung *= (double)tabelJumlahMain[a] / maxJmlMain; // factoring jumlah main
                tabelKontrol[a] = (int)Math.Ceiling(jmlAngklung);
            }
            int jmlAngklungTotal = 0;
            foreach (int jmlAngklung in tabelKontrol.Values)
            {
                jmlAngklungTotal += jmlAngklung;
            }
            const int RATARATA = 3;
            while (jmlAngklungTotal < jmlPemain * RATARATA)
            {
                foreach (Angklung a in tabelJumlahMain.Keys)
                {
                    tabelKontrol[a] = (int)Math.Ceiling(tabelKontrol[a] * 1.2);
                }
                jmlAngklungTotal = 0;
                foreach (int jmlAngklung in tabelKontrol.Values)
                {
                    jmlAngklungTotal += jmlAngklung;
                }
            }
            while (jmlAngklungTotal > jmlPemain * RATARATA)
            {
                foreach (Angklung a in tabelJumlahMain.Keys)
                {
                    if (tabelKontrol[a] > 1) tabelKontrol[a] = (int)Math.Floor(tabelKontrol[a] / 1.1);
                }
                jmlAngklungTotal = 0;
                foreach (int jmlAngklung in tabelKontrol.Values)
                {
                    jmlAngklungTotal += jmlAngklung;
                }
            }

            List<Angklung> angklungUtkDibagikan = new List<Angklung>();
            foreach (KeyValuePair<Angklung, int> kvp in tabelKontrol)
            {
                for (int i = 0; i < kvp.Value; i++) angklungUtkDibagikan.Add(kvp.Key);
            }
            // shuffle the list
            Random rng = new Random();
            int n = angklungUtkDibagikan.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                Angklung a = new Angklung(angklungUtkDibagikan[k]);
                angklungUtkDibagikan[k] = new Angklung(angklungUtkDibagikan[n]);
                angklungUtkDibagikan[n] = a;
            }  

            // initialize distribusi
            List<HashSet<Angklung>> tabelDistribusi = new List<HashSet<Angklung>>();
            for (int i = 0; i < jmlPemain; i++)
            {
                tabelDistribusi.Add(new HashSet<Angklung>());
            }

            // PRECALCULATE VEKTOR BOBOT //
            // bobot: kebentrokan 4 kali lebih penting dibanding jumlah main
            // (kemiripan bisa dimasukin sini sih, tapi...ntar ajalah)
            const int NUM_OF_FACTORS = 2;
            double[,] matriksPerbandingan = new double[NUM_OF_FACTORS, NUM_OF_FACTORS] { { 1.0, 4.0 }, { 0.25, 1.0 } };
            double[,] matriksTernormalisasi = new double[NUM_OF_FACTORS, NUM_OF_FACTORS];
            // normalisasi matriks perbandingan
            for (int col = 0; col < NUM_OF_FACTORS; col++)
            {
                double colSum = 0;
                for (int row = 0; row < NUM_OF_FACTORS; row++)
                {
                    colSum += matriksPerbandingan[row, col];
                }
                for (int row = 0; row < NUM_OF_FACTORS; row++)
                {
                    matriksTernormalisasi[row, col] = matriksPerbandingan[row, col] / colSum;
                }
            }
            // membuat vektor bobot
            double[] vektorBobot = new double[NUM_OF_FACTORS];
            for (int row = 0; row < NUM_OF_FACTORS; row++)
            {
                double jml = 0;
                for (int col = 0; col < NUM_OF_FACTORS; col++)
                {
                    jml += matriksTernormalisasi[row, col];
                }
                vektorBobot[row] = jml;
            }

            // threshold ketimpangan
            const int THRESHOLD_KETIMPANGAN = 2;

            double prog = 0;
            double total = angklungUtkDibagikan.Count;

            // main loop:
            // bagikan angklung sampai semua sudah dibagikan
            while (angklungUtkDibagikan.Count > 0)
            {
                Angklung a = angklungUtkDibagikan[0];
                angklungUtkDibagikan.RemoveAt(0);
                bool sudahDibagikan = false;
                foreach (HashSet<Angklung> pemain in tabelDistribusi)
                {
                    if (pemain.Count == 0)
                    {
                        pemain.Add(a);
                        //foreach (List<Tuple<Angklung, bool>> ketuk in partiturCheck)
                        //{
                        //    for (int i = 0; i < ketuk.Count; i++)
                        //    {
                        //        if (ketuk[i].Item1 == a) ketuk[i] = new Tuple<Angklung, bool>(a, true);
                        //    }
                        //}
                        sudahDibagikan = true;
                        break;
                    }
                }
                if (!sudahDibagikan)
                {
                    // cari siapa yang pegang paling dikit
                    int minAngkl = Int32.MaxValue;
                    foreach (HashSet<Angklung> pemain in tabelDistribusi)
                    {
                        if (minAngkl > pemain.Count) minAngkl = pemain.Count;
                    }
                    // tentukan siapa yang akan dapat dengan Fuzzy MCDM
                    int alternatifTerpilih = -1;
                    double nilaiAlternatifTerpilih = -1;
                    for (int i = 0; i < tabelDistribusi.Count; i++)
                    {
                        HashSet<Angklung> pemain = tabelDistribusi[i];
                        if (!pemain.Contains(a) && pemain.Count < minAngkl+THRESHOLD_KETIMPANGAN)
                        {
                            HashSet<Angklung> setAngklungIni = new HashSet<Angklung>(pemain);
                            setAngklungIni.Add(a);
                            Tuple<double, double> rasioPemainIni = CountRasioBentrokMain(setAngklungIni);
                            double bobotRasioBentrok = Math.Pow(rasioPemainIni.Item1, vektorBobot[0]);
                            double bobotRasioMain = Math.Pow(100 - rasioPemainIni.Item2, vektorBobot[1]);
                            double bobotRasio = Math.Min(bobotRasioBentrok, bobotRasioMain);
                            if (nilaiAlternatifTerpilih < bobotRasio)
                            {
                                alternatifTerpilih = i;
                                nilaiAlternatifTerpilih = bobotRasio;
                            }
                        }
                    }
                    tabelDistribusi[alternatifTerpilih].Add(a);
                    // begitu tahu siapa yg dapat, update tabel partiturCheck
                    //LinkedListNode<List<Tuple<Angklung, bool>>> node = partiturCheck.First;
                    //while (node != null)
                    //{
                    //    bool sudahAda = false;
                    //    List<Tuple<Angklung, bool>> ketukExtended = new List<Tuple<Angklung,bool>>(node.Value);
                    //    if (node.Previous != null) ketukExtended.AddRange(node.Previous.Value);
                    //    if (node.Next != null) ketukExtended.AddRange(node.Next.Value);
                    //    foreach (Tuple<Angklung, bool> angk in ketukExtended)
                    //    {
                    //        // apakah dia memainkan angklung lain di ketuk yg sama/dekat?
                    //        if (angk.Item1 != a && tabelDistribusi[alternatifTerpilih].Contains(angk.Item1))
                    //        {
                    //            sudahAda = true;
                    //            break;
                    //        }
                    //    }
                    //    for (int i = 0; i < node.Value.Count; i++)
                    //    {
                    //        // di seluruh partitur, cari angklung yg mau ditambahin ini
                    //        if (node.Value[i].Item1 == a)
                    //        {
                    //            // jika dia tidak memainkan angklung lain di ketuk yg sama/dekat, maka tandai sbg bisa dimainkan
                    //            if (!sudahAda) node.Value[i] = new Tuple<Angklung, bool>(a, true);
                    //            // jika sudah main yg lain, tambahkan lagi angklung ini untuk dibagikan ke org lain (jika memang blm ditambahkan)
                    //            else if (angklungUtkDibagikan.First.Value != a) angklungUtkDibagikan.AddFirst(a);
                    //        }
                    //    }
                    //    node = node.Next;
                    //}
                }

                prog = total - angklungUtkDibagikan.Count;
                double ratprog = (prog / total) * 100;
                worker.ReportProgress((int)ratprog);
            }

            List<Pemain> hasilDistribusi = new List<Pemain>();
            int namaPemain = 1;
            foreach (HashSet<Angklung> setAngklung in tabelDistribusi)
            {
                hasilDistribusi.Add(new Pemain(namaPemain.ToString(), setAngklung));
                namaPemain++;
            }
            return hasilDistribusi;
        }

        public static Tuple<double, double> CountRasioBentrokMain(IEnumerable<Angklung> listAngklung)
        {
            Partitur partiturToProcess = new Partitur("");
            foreach (Partitur p in PartiturCollection.Instance)
            {
                partiturToProcess.PartiturAbsolut.AddRange(p.PartiturAbsolut);
                partiturToProcess.isAbsolutProcessed = true;
            }

            Dictionary<Angklung, HashSet<int>> tabelLokasiMain = new Dictionary<Angklung, HashSet<int>>();
            for (int ketukIdx = 0; ketukIdx < partiturToProcess.PartiturAbsolut.Count; ketukIdx++)
            {
                foreach (Angklung nada in partiturToProcess.PartiturAbsolut[ketukIdx])
                {
                    if (!tabelLokasiMain.ContainsKey(nada))
                    {
                        HashSet<int> newList = new HashSet<int>();
                        newList.Add(ketukIdx);
                        tabelLokasiMain.Add(nada, newList);
                    }
                    else
                    {
                        tabelLokasiMain[nada].Add(ketukIdx);
                    }
                }
            }

            HashSet<int> setLokasiMain = new HashSet<int>();
            HashSet<int> setLokasiBentrok = new HashSet<int>();
            HashSet<string> angklungSudahDihitung = new HashSet<string>();
            foreach (Angklung angklung in listAngklung)
            {
                setLokasiMain.UnionWith(tabelLokasiMain[angklung]);
                foreach (Angklung angklung_2 in listAngklung)
                {
                    string str_c1 = angklung.ToString() + ":" + angklung_2.ToString();
                    string str_c2 = angklung_2.ToString() + ":" + angklung.ToString();
                    if (angklung != angklung_2 && !angklungSudahDihitung.Contains(str_c1) && !angklungSudahDihitung.Contains(str_c2))
                    {
                        angklungSudahDihitung.Add(str_c1);
                        HashSet<int> kemunculanExtended = new HashSet<int>();
                        foreach (int lokA in tabelLokasiMain[angklung])
                        {
                            kemunculanExtended.Add(lokA);
                            kemunculanExtended.Add(lokA + 1);
                            kemunculanExtended.Add(lokA - 1);
                        }
                        kemunculanExtended.IntersectWith(tabelLokasiMain[angklung_2]);
                        setLokasiBentrok.UnionWith(kemunculanExtended);
                    }
                }
            }

            double rasioMain = 0, rasioBentrok = 0;
            if (setLokasiMain.Count > 0)
            {
                rasioMain = (double)setLokasiMain.Count / partiturToProcess.PartiturAbsolut.Count;
                rasioMain = Math.Round(rasioMain * 100, 3);
                rasioBentrok = (double)setLokasiBentrok.Count / setLokasiMain.Count;
                rasioBentrok = Math.Round(rasioBentrok * 100, 3);
            }

            return new Tuple<double, double>(rasioBentrok, rasioMain);
        }
    }
}
